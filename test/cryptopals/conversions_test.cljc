(ns cryptopals.conversions-test
  (:require [cryptopals.conversions :as cv]
            [clojure.string :refer [join]]
            [clojure.test :refer [deftest is]]))

(deftest int->bin-test
  (is (= (cv/int->bin 64)
         "1000000"))
  (is (= (cv/int->bin 63)
         "111111"))
  (is (= (cv/int->bin -1)
         "-1"))
  (let [n (cv/int->bin 0x10000)]
    (is (= (count n) 17))
    (is (= (first n) \1))
    (is (= (subs n 1)
           (join (repeat 16 \0))))))

(deftest bin->int-test
  (is (== (cv/bin->int "10")
          2))
  (is (== (cv/bin->int "-1")
          -1))
  (is (== (cv/bin->int "10000")
          16))
  (is (== (cv/bin->int (apply str "1" (repeat 63 "0")))
          9223372036854775808)))

(deftest bytes->hex-test
  (is (= (cv/bytes->hex (range 1 7))
         "010203040506"))
  (is (= (cv/bytes->hex '(0 0xaa 0xbb 0xcc))
         "00aabbcc"))
  (is (= (cv/bytes->hex '(0))
         "00"))
  (is (= (cv/bytes->hex '())
         "")))

(deftest hex->bytes-test
  (is (= (cv/hex->bytes "313233343536373839")
         (range 0x31 0x3a)))
  (is (= (cv/hex->bytes "00")
         '(0)))
  (is (= (cv/hex->bytes "0ffff")
         '(0 255 255)))
  (is (= (cv/hex->bytes "0fff")
         '(15 255))))

(deftest bytes->int-test
  (is (== (cv/bytes->int (range 1 7))
          0x010203040506))
  (is (== (cv/bytes->int '(0 0xaa 0xbb 0xcc))
          0xaabbcc))
  (is (== (cv/bytes->int '(0))
          0))
  (is (== (cv/bytes->int '())
          0)))

(deftest int->bytes-test
  (is (= (cv/int->bytes (.pow (biginteger 2) 64))
         '(1 0 0 0 0 0 0 0 0)))
  (is (= (cv/int->bytes 1)
         '(1)))
  (is (= (cv/int->bytes 35)
         '(35)))
  (is (= (cv/int->bytes 257)
         '(1 1))))

(deftest hex->int-test
  (is (= (cv/hex->int "10")
         16))
  (is (= (cv/hex->int "0")
         0))
  (is (= (cv/hex->int "1a")
         26))
  (is (= (cv/hex->int "ffff")
         65535)))

(deftest hex->text-test
  (is (= (cv/hex->text "7468697320697320612074657374")
         "this is a test"))
  (is (= (cv/hex->text "313233343536373839")
         "123456789"))
  (is (= (cv/hex->text "776f6b6b6120776f6b6b61212121")
         "wokka wokka!!!")))

(deftest text->hex-test
  (is (= (cv/text->hex "this is a test")
         "7468697320697320612074657374"))
  (is (= (cv/text->hex "123456789")
         "313233343536373839"))
  (is (= (cv/text->hex "wokka wokka!!!")
         "776f6b6b6120776f6b6b61212121")))

(deftest bytes->text-test
  (is (= (cv/bytes->text (range 97 123))
         "abcdefghijklmnopqrstuvwxyz"))
  (is (= (cv/bytes->text (range 48 58))
         "0123456789")))

(deftest text->bytes-test
  (is (= (cv/text->bytes "abcdefghijklmnopqrstuvwxyz")
         (range 97 123)))
  (is (= (cv/text->bytes "0123456789")
         (range 48 58))))

(deftest HEX->hex-test
  (is (= (cv/HEX->hex "080A0FD2")
         "080a0fd2"))
  (is (= (cv/HEX->hex "313233")
         "313233")))

(deftest hex->HEX-test
  (is (= (cv/hex->HEX "080a0fd2")
         "080A0FD2"))
  (is (= (cv/hex->HEX "313233")
         "313233")))

(deftest hex->64-test
  (is (= (cv/hex->64 "10") "EA=="))
  (is (= (cv/hex->64 "40") "QA=="))
  (is (= (cv/hex->64 "100") "AQA="))
  (is (= (cv/hex->64 "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d")
        "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t")))

(deftest base64->int-test
  (is (= (cv/base64->int "TWFu")
         0x4d616e))
  (is (= (cv/base64->int "TWE=")
         (cv/base64->int "TWE")
         0x4d61))
  (is (= (cv/base64->int "TQ==")
         (cv/base64->int "TQ")
         0x4d))
  (is (= (cv/hex->text
          (format "%x"
            (biginteger
             (cv/base64->int
              "TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0aGlzIHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1c3Qgb2YgdGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0aGUgY29udGludWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdlLCBleGNlZWRzIHRoZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4="))))
        "Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure.")))
