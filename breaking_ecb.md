# Breaking ECB

Electronic Code Book (ECB) is an encryption/decryption mode of operation, like CBC, but less secure.

The weakness with ECB is that a given block will always be encrypted the same way wherever it is in the original text :

|      Plaintext     |    Ciphertext   |
| :----------------: | :-------------: |
| `theFirstSentence` |   Gibberish 1   |
| `someSentenceHere` | Gibberish **2** |
| `differentStuffxx` |   Gibberish 3   |
| `someSentenceHere` | Gibberish **2** |

We use that weakness in the challenges 12 and 14: by repeatedly sending variable-length text to a function that puts our text before the target text.<br>
_Note:_ In this whole explaining file, I will use
* 'A' to note attacker-controlled text, in my code it's a sequence of zero-bytes
* 'T' to note target-text
* 'P' to note padding bytes
* 'B' to note prefix bytes ('B' as in Before)

When I talk about complexity, it's a function of the number of calls to the encryption algorithm.

I will now go over the algorithm for the challenge #12, and then describe what we need to change in the challenge #14.
## Step 1: Block cipher size
The first thing we need to know to be able to do anything is the **block cipher size**.
Fortunately, the method to get it is extremely simple : since all text to be encoded by AES needs to be padded up to that size, we just send an increasing-length gibberish-text and wait to see an increase in the size of the final text :

| Size of attack text | Pre-cipher full text | Post-cipher text size |
| :-----------------: | :------------------: | :-------------------: |
|         0           |  `TTTTTTTTTTPPPPPP`  |          16           |
|         1           |  `ATTTTTTTTTTPPPPP`  |          16           |
|         2           |  `AATTTTTTTTTTPPPP`  |          16           |
|         6           |  `AAAAAATTTTTTTTTT`  |          16           |
|      7 part 1       |  `AAAAAAATTTTTTTTT`  |                       |
|      7 part 2       |  `TPPPPPPPPPPPPPPP`  |          32           |

The increase in size is due to the fact that the size of A+T has gone above n\*block_size, and some of the text is now in a new line, with a ton of padding. We note the difference in size between the result of steps 6 and 7 (here), and that is the block cipher size.

### Computing time:
We send text to be encrypted up to size-1 times. Since size is a constant, and in the case of AES always 16, it's really fast.

## Step 2: Brute force characters
To actually decipher the target text, we need to send a few special lines and analyse the results:

1. We first send size-1 null-bytes. The first line of the resulting cipher text is then: all but the last bytes are null, and the last one is the first byte of the text.
    (first line in the following array)
2. We then repeatedly send size-1 null-bytes and one 'guess' byte. Each time, we check the resulting cipher against the original cipher.
3. When we find an equality in ciphers, it means we have found the first letter (byte) of the target text.
4. We can now cycle back to step 1 but this time we first send size-2 null-bytes, and in step 2 we send size-2 null-bytes + what we already have found + the guess.

Here is an example when the target text is "error in connection"

| Size of attack text | Pre-cipher first line | Cipher first line |
| :-----------------: | :-------------------: | :---------------: |
|      size - 1       |   `AAAAAAAAAAAAAAAT`  |  **Gibberish 1**  |
|        size         |   `AAAAAAAAAAAAAAAa`  |    Gibberish ?    |
|        size         |   `AAAAAAAAAAAAAAAb`  |    Gibberish ?    |
|        size         |   `AAAAAAAAAAAAAAAc`  |    Gibberish ?    |
|        size         |   `AAAAAAAAAAAAAAAd`  |    Gibberish ?    |
|        size         |   `AAAAAAAAAAAAAAAe`  |  **Gibberish 1**  |
|      size - 2       |   `AAAAAAAAAAAAAATT`  |  **Gibberish 2**  |
|        size         |   `AAAAAAAAAAAAAAea`  |    Gibberish ?    |
|        size         |   `AAAAAAAAAAAAAAeb`  |    Gibberish ?    |
|        size         |   `AAAAAAAAAAAAAAec`  |    Gibberish ?    |
|        size         |          ...          |    Gibberish ?    |
|        size         |   `AAAAAAAAAAAAAAer`  |  **Gibberish 2**  |
|      size - 3       |   `AAAAAAAAAAAAATTT`  |    Gibberish 3    |
|        size         |   `AAAAAAAAAAAAAera`  |    Gibberish ?    |
|        ....         |          ...          |        ...        |
|        ....         |          ...          |        ...        |
|        size         |   `error in connect`  |        ...        |

At the end we can see that a problem arises: What do we do when we have deciphered the first line (the first 16 characters) of the ciphertext?
Thankfully, the answer is quite simple: we just start over the same thing again, but instead of looking at the *first* line in the resulting cipher, we look at the *second*, and when we have discovered size\*2 bytes of the ciphertext we look at the *third* line, etc, etc... (see following array)

| Size of attack text | Pre-cipher 1st/2nd line | Cipher 1st/2nd line |
| :-----------------: | :---------------------: | :-----------------: |
|      size - 1       |    `AAAAAAAAAAAAAAAT`   |         ...         |
|      (part 2)       |    `TTTTTTTTTTTTTTTT`   |   **Gibberish 1**   |
|      size * 2       |    `AAAAAAAAAAAAAAAe`   |         ...         |
|      (part 2)       |    `rror in connecta`   |     Gibberish ?     |
|      size * 2       |    `AAAAAAAAAAAAAAAe`   |         ...         |
|      (part 2)       |    `rror in connectb`   |     Gibberish ?     |
|        ...          |           ...           |         ...         |
|        ...          |           ...           |         ...         |
|      size * 2       |    `AAAAAAAAAAAAAAAe`   |         ...         |
|      (part 2)       |    `rror in connecti`   |   **Gibberish 1**   |
|      size - 2       |    `AAAAAAAAAAAAAATT`   |         ...         |
|      (part 2)       |    `TTTTTTTTTTTTTTTT`   |   **Gibberish 2**   |
|        ...          |           ...           |         ...         |
|        ...          |           ...           |         ...         |

The important things to note here are:
* The number of null-bytes to send looks like this: [size-1, size-2... 1, 0, size-1, size-2...]
* After one series of null-bytes, we always send the part of the text we already know, to be able to align our guess to its proper place

### Computing time

If we have a text of n characters, and c the total number of valid characters that can be found in the ciphertext (letters and symbols), we send an average of n\*(c+1)/2 lines to the encoder.<br>
Although we can't get asymptotically better than this, we can reduce the factor (c+1)/2 by quite a lot with some optimizations.

### Optimizations

 * To reduce from c+1 to c, we can, before the brute force phase really begins, send size-1, then size-2... null-bytes and store the results to create a lookup table.
 * To reduce c itself, we can sort the characters in order of frequency in a standard text (the famous 'etaoinshrdlcu' we used earlier). This one trick saved me a lot of execution time.

# Breaking ECB with a prefix

In the challenge #14, the encryption box appends a random gibberish prefix before our input text. What this changes is that we will need to pad the prefix to isolate it from our text to be able to do anything.<br>
This is handled in 3 steps:

## Step 1: Finding the end of the prefix

Since we have no information whatsoever on what is inside the prefix or its size, the first thing to do is find where it ends.
To do this, we send 3\*size zero bytes, and we look for two successive identical lines in the output.<br>
The index of the first twin line is the index of the first line without any prefix byte in it.<br>
In case the prefix is actually full of zeroes, we also have to test with a different number.

## Step 2: Figuring out the length of the prefix

We will need to pad the prefix to entirely contain it into full lines so that we can apply the algorithm for challenge #12.
To do that we need to know how many bytes there are in the last line of the prefix.<br>
This is done by first sending 2\*size zero bytes, then 2 times minus 2, then 2 times minus 3, etc. When we find, at the index previously found, a line different from the initial one, we have found the size we were looking for.

## Step 3: The padding itself

Now that we know how long the prefix actually is, we need to pad it out of our way:<br>
Every time we send a few zero bytes and a guess in the algorithm, we will prefix that with the good number of null bytes to pad, and look at the right index in the output.<br>


So there you have it, breaking ECB step by step.
