(defproject cryptopals "15.0"
  :description "My attempt at solving a few cryptographic challenges of Cryptopals in Clojure"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/data.priority-map "1.0.0"]]
  :main cryptopals.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
