(ns cryptopals.conversions
  (:require [clojure.string :as str :refer [join starts-with? index-of]]
            [cryptopals.utils :refer [pad-bin]]))

(defn int->bin
  "Convert any integer into the binary string representing that number."
  [n]
  (.toString (biginteger n) 2))

(defn bin->int
  "Read a binary string into a signed integer."
  [b]
  (if (starts-with? b "-")
    (read-string (str "-2r" (subs b 1)))
    (read-string (str "2r" b))))

(defn bytes->hex
  "Convert a byte sequence into the number it represents, in hexadecimal."
  [s]
  (join (map #(format "%02x" %) s)))

(defn hex->bytes
  "Convert an hexadecimal string into a byte sequence."
  [hex]
  (map #(read-string (str "16r" (join %)))
    (partition 2 (pad-bin hex 2))))

(defn bytes->uint
  "Convert a byte sequence into an unsigned integer."
  [s]
  (BigInteger. (byte-array (cons 0 s))))

(defn bytes->int
  "Convert a byte sequence into a signed integer."
  [s]
  (if (seq s)
    (BigInteger. (byte-array s))
    0))

(defn int->bytes
  "Convert a signed integer into a byte sequence."
  [n]
  (map #(mod % 256) (.toByteArray (biginteger n))))

(defn hex->int
  "Decode hex-encoded string as integer."
  [s]
  (read-string (str "16r" s)))

(defn int->hex
  "Encode an integer into its hex string representation."
  [n]
  (format "%02x" n))

(defn bytes->text
  "Decode byte sequence 's' as text string."
  [s]
  (String. (byte-array (map unchecked-byte s)) "UTF-8"))

(defn text->bytes
  "Encode text string as byte sequence."
  [s]
  (map #(mod % 256) (.getBytes s "UTF-8")))

(defn hex->text
  "Decode hex-encoded string as text string."
  [s]
  (-> s hex->bytes bytes->text))

(defn text->hex
  "Encode text string as hexadecimal string."
  [s]
  (-> s text->bytes bytes->hex))

(defn HEX->hex
  "Convert a big hexa string into a lower case one."
  [s]
  (str/replace s #"[A-F]" #(.toLowerCase %)))

(defn hex->HEX
  "Convert a small hexa string into an upper case one."
  [s]
  (str/replace s #"[a-f]" #(.toUpperCase %)))

(def ^:private base64-chars
  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/")

(defn- three-bytes->64
  "Encode in 64 a triplet of bytes in hex.
  Helper for the 'hex->64' bigger function."
  [tb]
  (let [bin (-> tb
                join
                hex->int
                int->bin
                (pad-bin 8))]
    (->> bin
         (partition 6 6 "0000")
         (map (fn [sextet] (->> sextet
                                (apply str "2r")
                                read-string
                                (get base64-chars))))
         join
         (#(case (count %)
             4 %
             3 (str % "=")
             2 (str % "=="))))))

(defn hex->64
  "Convert a hex-encoded number into a string representing
  that number in base64."
  [s]
  (->> s
       (partition 6 6 "")
       (map three-bytes->64)
       join))

(defn- four-bytes->bin
  "Decode a padded base64 sequence of 4 characters and return its binary value.
  Helper for the 'base64->int' bigger function."
  [quartet]
  (let [[a b c d] (map (fn [ch] (->> ch
                                     (index-of base64-chars)
                                     (#(when % (int->bin %)))
                                     (#(when % (pad-bin % 6)))))
                       quartet)]
    (cond
      (nil? c) (str a (subs b 0 2))
      (nil? d) (str a b (subs c 0 4))
      :else (str a b c d))))

(defn base64->int
  "Convert a base64-encoded number into an unsigned integer."
  [input]
  (->> input
       (partition 4 4 "==")
       (map four-bytes->bin)
       (apply str "2r")
       read-string))
